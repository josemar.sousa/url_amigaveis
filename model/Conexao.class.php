<?php

class Conexao extends Config{
    private $host;
    private $user;
    private $senha;
    private $banco;
    private static $conn;
    
    protected $obj;
    protected $itens=array();
    protected $prefix;
            
    function __construct() {
        $this->host = self::BD_HOST;
        $this->user = self::BD_USER;
        $this->senha = self::BD_SENHA;
        $this->banco = self::BD_BANCO;
        $this->prefix = self::BD_PREFIX;
        
        try {
            if($this->Conectar() == null){
                $this->Conectar();                
            }
        } catch (Exception $e) {
            exit($e->getMessage().'<h2>Erro ao conectar com o banco de dados!</h2>');
        }
    }
    
    private function Conectar(){
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        );
        if(is_null(self::$conn)){
            self::$conn = new PDO("mysql:host={$this->host};dbname={$this->banco}",
                    $this->user, $this->senha, $options );            
        }
        return self::$conn;
    }
    
    function ExecuteSQL($query, array $params = null){
        $this->obj = $this->Conectar()->prepare($query);
        if($params != NULL){
            foreach ($params as $key => $value){
                $this->obj->bindValue($key, $value);
            }
        }
        return $this->obj->execute();
    }
    
    function lastInsertId(){
        return $this->Conectar()->lastInsertId();
    }
    
    function Listar(){
        return $this->obj->fetch(PDO::FETCH_OBJ);
    }
    
    function ListarAll(){
        return $this->obj->fetchAll(PDO::FETCH_OBJ);
    }
    /*
    function TotalDados(){
        return $this->obj->rowCount();
    }
    
    function GetItens(){
        return $this->itens;
    }*/
}