<?php

class Usuarios extends CrudGenerico{
    
    protected $tabela = 'usuarios';
    
    public function buscarTotalUsuarios(){
        $sql = "SELECT COUNT(*) AS total FROM $this->tabela";
        Conexao::ExecuteSQL($sql);
        return $this->Listar();
    }
    
    public function buscarUsuarioSenhaAtual($cpf, $senha, $novaSenha){
        $senhaMd5 = md5($senha);
        $sql = "SELECT * FROM $this->tabela WHERE cpf = :cpf AND senha = :senha LIMIT 1";
        $params = array(
            ':cpf' => $cpf,
            ':senha' => $senhaMd5
        );
        Conexao::ExecuteSQL($sql, $params);
        if($this->Listar()){
            $this->updateSenha($cpf, md5($novaSenha));
        }else{
            $_SESSION['erro'] = 'Senha atual não confere..';
        }
    }
    
    public function updateSenha($cpf, $senha){
        $sql = "UPDATE $this->tabela SET senha = :senha WHERE cpf = :cpf";
        $params = array(
            ':cpf' => $cpf,
            ':senha' => $senha
        );
        $execute = Conexao::ExecuteSQL($sql, $params);
        
        if($execute){
            $_SESSION['sucesso'] = 'Senha alterada com sucesso!';
        }else{
            $_SESSION['erro'] = 'Erro ao trocar a senha, por favor tente novamente!';
        }
    }
    
    public function buscarUsuarioPorId($id) {
        $sql = "SELECT * FROM $this->tabela WHERE id=:id ";
        $params = array(':id' => (int)$id);
        Conexao::ExecuteSQL($sql, $params);
        return $this->ListarAll();
    }
       
    
}