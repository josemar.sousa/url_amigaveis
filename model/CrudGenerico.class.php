<?php

abstract class CrudGenerico extends Conexao {

    public $id_empresa;
    public $limit = null;

    function __construct() {
        parent::__construct();
    }
    /*function __construct() {
        $this->id_empresa = $_SESSION['empresas_id'];
    }*/

    public function insert($attributes) {
        $keys = array_keys($attributes);
        $camposTabela = implode(',', $keys);
        $values = null;
        foreach ($keys as $key) {
            $values .= ',:' . $key;
        }
        $values = (trim(ltrim($values, ',')));

        $sql = "INSERT INTO $this->tabela($camposTabela) values($values)";
        
        Conexao::ExecuteSQL($sql, $attributes);
        
        return $this->lastInsertId();
    }

    public function update($attributes) {
        $retorno = FALSE;
        $keys = array_keys($attributes);
        $camposTabela = implode(',', $keys);
        $values = null;
        foreach ($keys as $key) {
            $values .= ',' . $key . '=:' . $key;
        }
        $id = $attributes['id'];

        $values = (trim(ltrim($values, ',')));

        $sql = "UPDATE $this->tabela set $values WHERE id=:id";
        Conexao::ExecuteSQL($sql, $attributes);
        $retorno = TRUE;
        return $retorno;
    }

    public function delete($id) {
        $sql = "DELETE FROM $this->tabela WHERE id=:id";        
        $params = array(':id' => (int)$id);
        Conexao::ExecuteSQL($sql, $params);
        return TRUE;
    }

    public function findAll() {
        $sql = "SELECT * FROM $this->tabela";
        Conexao::ExecuteSQL($sql);
        return $this->ListarAll();
    }

    public function buscarPorId($id) {
        $sql = "SELECT * FROM $this->tabela WHERE id=:id ";
        $params = array(':id' => (int)$id);
        Conexao::ExecuteSQL($sql, $params);
        return $this->Listar();
    }

    /*public function buscarPorIdEmpresa() {
        $id = $_SESSION['empresas_id'];
        $sql = "SELECT * FROM $this->tabela WHERE empresas_id = '$id' ";
        $stm = DB::prepare($sql);
        $stm->execute();
        return $stm->fetch();
    }

    public function updateStatus($status) {
        $id = $_SESSION['empresas_id'];
        $sql = "UPDATE $this->tabela SET `status`='$status' WHERE $this->tabela.empresas_id = $id";
        $stm = DB::prepare($sql);
        return $stm->execute();
    }*/

}
