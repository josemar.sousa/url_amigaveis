<?php

class Rotas{
    
    public static $pag;
    private static $pasta_controller = 'controller';
    private static $pasta_view = 'view';    
    private static $pasta_adm = 'adm';    
    
    /*    
    static function get_ImagePasta(){
        return 'media/img/';
    }
    
    static function get_ImageURL(){
        return self::get_SiteHOME() . '/'. self::get_ImagePasta();
    }
    
    static function ImageLink($img, $largura, $altura){
        $imagem = self::get_ImageURL() . "thumb.php?src={$img}&w={$largura}&h={$altura}&zc=1";
        return $imagem;
    }*/
    
    static function get_SiteHOME(){
        return Config::SITE_URL . '/' . Config::SITE_PASTA;
    }
    
    static function get_SiteRAIZ(){
        return $_SERVER['DOCUMENT_ROOT'] . '/' . Config::SITE_PASTA;
    }
    
    static function get_SiteTEMA(){
        return self::get_SiteHOME() . '/' . self::$pasta_view; //self::get_SiteHOME() = $this->get_SiteHOME()
    }
    
    static function pag_LoginADM(){
        return self::get_SiteHOME() .'/'. self::$pasta_adm .  '/login';
    }
    
    static function pag_Login(){
        return self::get_SiteHOME() . '/login';
    }
    
    static function pag_Home(){
        return self::get_SiteHOME() . '/home';
    }
    
    static function pag_Conteudo(){
        return self::get_SiteHOME() . '/conteudo';
    }
    
    static function pag_Usuarios(){
        return self::get_SiteHOME() . '/usuario';
    }
    
    static function pag_UsuariosPerfil(){
        return self::get_SiteHOME() . '/usuario-perfil';
    }
    
    static function pag_Empresa(){
        return self::get_SiteHOME() . '/empresa';
    }
    
        
    static function get_Pagina(){
        $diretorio = '';
        if(isset($_GET['pag'])){
            
            $pagina = $_GET['pag'];            
            self::$pag = explode('/', $pagina);
            
            for ($i = 0; $i < count(Rotas::$pag) - 1; $i++) {
                $diretorio = '../' . $diretorio;
            }
            
            if (self::$pag[0] == 'adm'){
                if(!empty(self::$pag[1])){
                    $pagina = 'controller/' . self::$pag[0] . '/'. self::$pag[1] . '.php';                                        
                }else{
                    header("Location: ".Rotas::pag_LoginADM());
                }
            }else{
                //Principal
                $pagina = 'controller/' . self::$pag[0] . '.php';                
            }
            
            //$pagina = 'controller/' . $_GET['pag'] . '.php';
            if(file_exists($pagina)){
                include $pagina;
            }else{
                include 'view/404.tpl';
            }
        }else{
            header("Location: ".Rotas::pag_Login());
        }        
    }
}