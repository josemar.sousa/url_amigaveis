<?php

class Paciente extends CrudGenerico{
    
    protected $tabela = 'pacientes';
    
    
    public function buscarPacientesEmAvaliacao(){
        $dataAtual = date('Y-m-d');
        
        $sql = "SELECT COUNT(*) AS em_avaliacao FROM $this->tabela WHERE fim_avaliacao >= '$dataAtual'";        
        Conexao::ExecuteSQL($sql);
        return $this->Listar();
    }
   
    public function buscarPacientesFimAvaliacao(){
        $dataAtual = date('Y-m-d');
        
        $sql = "SELECT COUNT(*) AS fim_avaliacao FROM $this->tabela WHERE fim_avaliacao < '$dataAtual'";
        Conexao::ExecuteSQL($sql);
        return $this->Listar();
    }
    
    public function buscarPacientesAno(){
        $dataAtual = date('Y-m-d');
        
        $sql = "SELECT COUNT(*) AS ano FROM $this->tabela WHERE YEAR(inicio_avaliacao) = '$dataAtual'";
        Conexao::ExecuteSQL($sql);
        return $this->Listar();
    }
    
    public function buscarTotalPacientes(){
        $sql = "SELECT COUNT(*) AS total FROM $this->tabela";
        Conexao::ExecuteSQL($sql);
        return $this->Listar();
    }
    
    public function todosAnosCadastroPaciente(){  
        $sql = "SELECT distinct extract(year from dt_cadastro) as ano FROM $this->tabela ORDER BY ano DESC;";
        Conexao::ExecuteSQL($sql);
        return $this->ListarAll();
    }
    
    public function ultimoAnoCadastroPaciente(){  
        $sql = "SELECT distinct extract(year from dt_cadastro) as ano FROM $this->tabela ORDER BY ano DESC;";
        Conexao::ExecuteSQL($sql);
        return $this->Listar();
    }
    
    public function mesAnoCadastroPaciente($mes, $ano){  
        $sql = "SELECT count(*) AS total_cadastro_mes_ano FROM $this->tabela WHERE MONTH(dt_cadastro) = '$mes' AND YEAR(dt_cadastro) = '$ano';";
        Conexao::ExecuteSQL($sql);
        return $this->Listar();
    }
    
}