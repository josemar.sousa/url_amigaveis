<?php

class Login extends CrudGenerico{
    
    private $cpf, $senha;
    protected $tabela = 'usuarios';
    
    public function __construct() {
            parent::__construct();
    }
    
    public function validarLogin($cpf, $senha){
        $this->setCpf($cpf);
        $this->setSenha($senha);
        
        $sql = "SELECT * FROM $this->tabela WHERE cpf = :cpf AND senha = :senha LIMIT 1";
        
        $params = array(
            ':cpf' => $this->getCpf(),
            ':senha' => $this->getSenha()
        );
        
        $stm = Conexao::ExecuteSQL($sql, $params);
        
        $result = $this->Listar();
        if($result){
            $_SESSION['logado'] = TRUE;
            $_SESSION['id_usuario'] = $result->id;
            $_SESSION['id_empresa'] = $result->empresas_id;
            $_SESSION['usuario_nome'] = $result->nome;
            $_SESSION['usuario_foto'] = $result->foto;
            
            $return = TRUE;
        }else{
            $_SESSION['erro'] = 'CPF ou senha inválidos.';
            $return = FALSE;
        }
        return $return;
    }
    
    public function logout($sessao){
        if(isset($_SESSION[$sessao])){
            unset($_SESSION[$sessao]);
            session_destroy();
            header("Location: ".Rotas::pag_Login());
        }
    }
    
    static function criptografia($valor){
        return md5($valor);
        //return hash('sha', $valor);
    }
    
    function getCpf() {
        return $this->cpf;
    }

    function getSenha() {
        return $this->senha;
    }

    private function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    private function setSenha($senha) {
        $this->senha = self::criptografia($senha);
    }


       
    
}