<?php

class Config{
    
    //INFORMAÇÃO BÁSICAS DO SITE
    const SITE_URL = "http://localhost";
    const SITE_PASTA = "url_amigaveis";
    const SITE_NOME = "URL Amigavéis";
    const SITE_EMAIL_ADM = "josemaar.sousa@gmail.com";
    
    //INFORMAÇÕES DO BANCO DE DADOS
    const BD_HOST = "localhost";
    const BD_USER = "root";
    const BD_SENHA = "";
    const BD_BANCO = "av3";
    const BD_PREFIX = "ct_";
    
    //INFORMAÇÃO PARA O PHP MAILLER
    const EMAIL_HOST = "smtp.gmail.com";
    const EMAIL_USER = "teste@gmail.com";
    const EMAIL_NOME = "Teste";
    const EMAIL_SENHA = "teste";
    const EMAIL_PORTA = 587;
    const EMAIL_SMTPAUTH = true;
    const EMAIL_SMTSECURE = "tls";
    const EMAIL_COPIA = "teste@gmail.com";
} 


