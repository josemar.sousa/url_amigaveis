<?php
if(isset($_SESSION['logado'])){
    $Usuarios = new Usuarios();
    
if (isset($_POST['cadastrar']) || isset($_POST['editar'])){
    $id = (isset($_POST['id'])) ? $_POST['id'] : null;
    $s = '123';
    $senha = md5($s);
    
    $dados = array(
        'id'=> $id,
        'senha'=> $senha,
        'nome'=> str_replace(".", "", $_POST['nome']),
        'sobrenome'=> str_replace(".", "", $_POST['sobrenome']),
        'email'=> $_POST['email'],
        'cpf'=> $_POST['cpf'],
        'crp'=> $_POST['crp'],
        'perfil'=> $_POST['perfil'],
        'cargo'=> $_POST['cargo'],
        'pos_graduacao'=> $_POST['pos_graduacao'],
        'pos_1'=> $_POST['nome_pos'],
        'empresas_id'=> $_POST['empresas_id']
    );
}

if (isset($_POST['cadastrar'])){    
    $Usuarios->insert($dados);
    $_SESSION['sucesso'] = 'Usuário cadastrado com sucesso!';
    $usuarios = $Usuarios->findAll();
    include 'view/usuario/listar_dados.tpl';
}else if(isset ($_POST['editar'])){
    $Usuarios->update($dados);
    $_SESSION['sucesso'] = 'Usuário alterado com sucesso!';
    $usuarios = $Usuarios->findAll();
    include 'view/usuario/listar_dados.tpl';
}else if(isset ($_POST['excluir'])){
    $Usuarios->delete($_POST['id']);
    $_SESSION['sucesso'] = 'Usuário excluído com sucesso!';
    $usuarios = $Usuarios->findAll();
    include 'view/usuario/listar_dados.tpl';
}else{
    $usuarios = $Usuarios->findAll();
    include 'view/usuario/usuario.tpl';
}

}else{
    header("Location: ".Rotas::pag_Login());
}