<?php

if(isset($_SESSION['logado'])){
    
    $Empresa = new Empresa();
    $UploadLogo = new UploadImagem();
    $Usuario = new Usuarios();
    $Paciente = new Paciente();

    $usuario = $Usuario->buscarTotalUsuarios();
    $pacientesEmAvaliacao = $Paciente->buscarPacientesEmAvaliacao();
    $pacientesFimAvaliacao = $Paciente->buscarPacientesFimAvaliacao();
    $pacientesAno = $Paciente->buscarPacientesAno();
    $pacientesTotal = $Paciente->buscarTotalPacientes();

    if(isset($_POST['id'])){
        $id_empresa = $_POST['id'];
    }

    if(isset($_POST['editarPerfil'])){
        $dados = array(
            'id' => $id_empresa,
            'nome' => $_POST['nome'],
            'descricao' => $_POST['descricao']
        );
        $Empresa->update($dados);
        $activePerfil = 'active';
        $_SESSION['sucesso'] = "Perfil alterado com sucesso!";
    }

    if(!empty($_FILES)){
        //$UploadLogo->width = 350;
        //$UploadLogo->height = 90;    
        $UploadLogo->salvar("ui/logotipos/", $_FILES['logoTipo'], $id_empresa);
        $activeLogo = 'active';
    }

    $empresa = $Empresa->buscarPorId($_SESSION['id_empresa']);//buscar pela session
    include 'view/empresa/empresa.tpl';
    
}else{
    header("Location: ".Rotas::pag_Login());
}