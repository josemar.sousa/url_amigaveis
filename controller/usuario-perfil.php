<?php

if(isset($_SESSION['logado'])){
    
$Usuarios = new Usuarios();
$UploadFoto = new UploadUserFoto();

if(isset($_POST['id'])){
    $id_usuario = $_POST['id'];
}

if (isset($_POST['editar'])){    
    $dados = array(
        'id'=> $id_usuario,
        'nome'=> str_replace(".", "", $_POST['nome']),
        'sobrenome'=> str_replace(".", "", $_POST['sobrenome']),
        'email'=> $_POST['email'],
        'cpf'=> $_POST['cpf'],
        'crp'=> $_POST['crp']
    );
    $_SESSION['sucesso'] = "Perfil alterado com sucesso!";
    $activePerfil = 'active';
    $Usuarios->update($dados);
}

if(!empty($_FILES)){
    //$UploadLogo->width = 350;
    //$UploadLogo->height = 90;    
    $UploadFoto->salvar("ui/fotoPerfil/", $_FILES['fotoPerfil'], $id_usuario);
    $activeFoto = 'active';
}

if(isset($_POST['alterarSenha'])){
    $validarSenha = $Usuarios->buscarUsuarioSenhaAtual($_POST['cpf'], $_POST['senhaAtual'], $_POST['novaSenha']);
    $activeSenha = 'active';
}

$usuarios = $Usuarios->buscarPorId($_SESSION['id_usuario']); //buscar pela session
$_SESSION['usuario_foto'] = $usuarios->foto;
$_SESSION['usuario_nome'] = $usuarios->nome;
include 'view/usuario-perfil/usuario-perfil.tpl';

}else{
    header("Location: index.php");
}