<?php

$Login = new Login();

if(isset($_POST['efetuarLogin'])){
    $cpf = $_POST['cpf'];
    $senha = $_POST['password'];
    $login = $Login->validarLogin($cpf, $senha);
    
    if($login){
        header("Location: ".Rotas::pag_Home());
    }else{
        header("Location: ".Rotas::pag_Login());
    }
    
}else if(isset (Rotas::$pag[1])){
    if(Rotas::$pag[1] == 'sair'){
        $Login->logout('logado');}
}else{
    include 'view/login.tpl';    
}


