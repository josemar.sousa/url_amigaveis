<?php
if(isset($_SESSION['logado'])){
    $Paciente = new Paciente();
    
    //echo Rotas::$pag[1];

    $pacientesEmAvaliacao = $Paciente->buscarPacientesEmAvaliacao();
    $pacientesFimAvaliacao = $Paciente->buscarPacientesFimAvaliacao();
    $pacientesAno = $Paciente->buscarPacientesAno();
    $pacientesTotal = $Paciente->buscarTotalPacientes();

    $pacienteAnosCadastro = $Paciente->todosAnosCadastroPaciente();
    include 'view/home/home.tpl';
}else{
    header("Location: ".Rotas::pag_Login());
}

//Gil