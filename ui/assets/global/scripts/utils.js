/*
*	Gerador e Validador de CPF v1.0.0
*	https://github.com/tiagoporto/gerador-validador-cpf
*	Copyright (c) 2014-2015 Tiago Porto (http://www.tiagoporto.com)
*	Released under the MIT license
*/
function CPF() { "user_strict"; function r(r) { for (var t = null, n = 0; 9 > n; ++n)t += r.toString().charAt(n) * (10 - n); var i = t % 11; return i = 2 > i ? 0 : 11 - i } function t(r) { for (var t = null, n = 0; 10 > n; ++n)t += r.toString().charAt(n) * (11 - n); var i = t % 11; return i = 2 > i ? 0 : 11 - i } var n = "CPF Inválido", i = "CPF Válido"; this.gera = function () { for (var n = "", i = 0; 9 > i; ++i)n += Math.floor(9 * Math.random()) + ""; var o = r(n), a = n + "-" + o + t(n + "" + o); return a }, this.valida = function (o) { for (var a = o.replace(/\D/g, ""), u = a.substring(0, 9), f = a.substring(9, 11), v = 0; 10 > v; v++)if ("" + u + f == "" + v + v + v + v + v + v + v + v + v + v + v) return n; var c = r(u), e = t(u + "" + c); return f.toString() === c.toString() + e.toString() ? i : n } }

var CPF = new CPF();

window.onload = function () {
    if (id('cpf')) {
            id('cpf').onkeyup = function () {
                    if (this.value.length <= 14) {
                            mascara(this, mCPF);
                    } else {

                            mascara(this, mCNPJ);
                    }
            };
            id('cpf').onblur = function () {

                    if (v_obj.value.length > 1 && v_obj.value.length < 14) {
                            v_obj.value = "";
                    } else if (v_obj.value.length > 14 && v_obj.value.length < 18) {
                            v_obj.value = "";
                    } else if ((CPF.valida($(this).val()) == "CPF Inválido") && v_obj.value.length == 14) {
                            alert(CPF.valida($(this).val()));
                            v_obj.value = "";
                    } else if (ValidarCNPJ($(this).val()) && v_obj.value.length == 18) {
                            alert("CNPJ Inválido");
                            v_obj.value = "";
                    }
            };
    };
}

/* Mascara Telefone */
function mascara(o, f) {
	v_obj = o
	v_fun = f
	setTimeout("execmascara()", 1)
}
function execmascara() {
	v_obj.value = v_fun(v_obj.value)


}

function mCPF(v) {
	v = v.replace(/\D/g, "")
	v = v.replace(/(\d{3})(\d)/, "$1.$2")
	v = v.replace(/(\d{3})(\d)/, "$1.$2")
	v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
	return v;
}

function id(el) {
	return document.getElementById(el);
}

