<div class="page-sidebar-wrapper"><!--fixed-->
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?php if (Rotas::$pag[0] == 'home'){echo 'active open';} ?>">
                <a href="<?php echo Rotas::pag_Home() ?>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Home</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Páginas</h3>
            </li>
            
<!--            <li class="nav-item <?php if ($pasta == 'paciente'){echo 'active open';}?>">
                <a href="paciente" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">Pacientes</span>
                </a>                
            </li>-->
            
<!--            <li class="nav-item">
                <a class="nav-link nav-toggle" onclick="abrirRelatorio()">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">Relatório</span>
                </a>                
            </li>-->
            
            <!--PERFIL ADMINISTRADOR-->            
            <li class="nav-item <?php if (Rotas::$pag[0] == 'usuario'){echo 'active open';}?>">
                <a href="<?php echo Rotas::pag_Usuarios() ?>" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Usuários</span>
                </a>                
            </li>
            <li class="nav-item <?php if (Rotas::$pag[0] == 'empresa'){echo 'active open';}?>">
                <a href="<?php echo Rotas::pag_Empresa() ?>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Perfil Empresa</span>
                </a>                
            </li>
            <!--PERFIL ADMINISTRADOR-->
                        
            <!--<li class="nav-item">
                <a href="notas" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Anotações</span>
                </a>                
            </li>-->
            <li class="nav-item  ">
                <a href="<?php echo  Rotas::pag_Login()?>/sair" class="nav-link nav-toggle">
                    <i class="icon-power"></i>
                    <span class="title">Sair</span>
                </a>                
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>