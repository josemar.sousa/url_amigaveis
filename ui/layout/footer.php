<!--[if lt IE 9]>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/excanvas.min.js"></script> 
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-3.3.1.min.js" type="text/javascript"></script>-->
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-inputmask/jquery.mask.js"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>


<!-- DatePicker-->
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>


<!-- SUMMERNOTE EDITOR - Bloco de Anotações -->
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- SUMMERNOTE EDITOR - Bloco de Anotações -->

<script src="<?php echo $diretorio ?>ui/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>


<script src="<?php echo $diretorio ?>ui/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- Google Maps -->
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script> -->
<!-- <script src="<?php echo $diretorio ?>ui/assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script> -->
<!-- END goolgle Maps -->

<script src="<?php echo $diretorio ?>ui/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>-->
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

<script src="<?php echo $diretorio ?>ui/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<!--<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>-->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo $diretorio ?>ui/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/form-input-mask.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/form-repeater.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<!--<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/timeline.min.js" type="text/javascript"></script>-->

<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/dashboard.js" type="text/javascript"></script>--><!--Gráfico na pasta home-->
<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/login.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/form-validation-md.min.js" type="text/javascript"></script>
<!--<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/ui-confirmations.min.js" type="text/javascript"></script>-->

<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo $diretorio ?>ui/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo $diretorio ?>ui/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
<!--<script src="<?php echo $diretorio ?>ui/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->
<!--<script src="<?php echo $diretorio ?>ui/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>-->

<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>

<!-- Google Maps -->
<!--<script src="<?php echo $diretorio ?>ui/assets/pages/scripts/maps-google.min.js" type="text/javascript"></script>
<!-- END Google Maps -->

<!-- END THEME LAYOUT SCRIPTS -->

<script>
    //FUNÇÃO EXECUTADA NO ATO DO CADASTRO - PÁGINA LOGIN
    $('input[name="tipo_conta"]').change(function () {
        categoria = this.value;
        $("#form_dados").css("display", "inline");
        $("#tipo_conta").css("display", "none");
        if (categoria == 1){
            $("#empresa_form").css("display", "inline");
            $("#autonomo_form").css("display", "none");
            $('#cadastrar_user').prop('disabled', false);
        }else if(categoria == 2){
            $("#autonomo_form").css("display", "inline");
            $("#empresa_form").css("display", "none");
            $('#cadastrar_user').prop('disabled', false);
        }
    });
    
    //FUNÇÃO EXECUTADA NO ATO DO CADASTRO - PÁGINA LOGIN
    $('#register-back-btn').click(function(){
        $("#form_dados").css("display", "none");
        $("#tipo_conta").css("display", "inline");
    });
    
    //FUNÇÃO EXECUTADA NO ATO DO CADASTRO DE USUÁRIO NA EMBRESA - PÁGINA USUARIO
    $('#pos_graduacao').change(function(){
       valor = this.value;
       if (valor == 'S'){
           $('#nomePos').css('display', 'inline');
           $('#nome_pos').prop('required', true);
       }else{
           $('#nomePos').css('display', 'none');
           $('#nome_pos').prop('required', false);
       }
    });
    
    //FUNÇÃO PARA ABRIR O RELATÓRIO EM UMA PÁGINA REDIMENSIONADA - MENU VERTICAL
    function abrirRelatorio(){
        window.open("relatorios/modelo1.php", " ", "height=790, width=850, toolbar=yes, menubar=yes, status=no, top=10, left=200");
    }
    
    /*CONFIRMAÇÃO PARA EXCLUSÃO*/
    $('[data-toggle=confirmation]').confirmation({
        btnOkClass: 'btn btn-success',
        btnCancelClass: 'btn btn-danger',
        onConfirm: function () {
            var id = $(this);
            var funcao = $(this).data('funcao');

            window[funcao](id);
        },
        onCancel: function () {
        }
    });
    
    /* EXCLUSÃO DINÂMICO */
    function excluir(obj) {
        var id = obj.data('id');
        $("#id").val(id);
        $("#formExcluir")[0].submit();
    }
    
    //FUNÇÃO PARA CARREGAR A LOGOTIPO NOS LUGARES ESPECIFICOS. DEPENDE DO READY EM CADA PÁGINA PARA A EXECUÇÃO DESTA FUNÇÃO - PÁGINAS RELATOIO E EMPRESA
    function attImagem(logo) {
        var img = logo;
        $.ajax({
            url: 'controller/carregaLogo.php',
            type: 'POST',
            data: {
                img: img
            },
            beforeSend: function() {
                $('#logotipo').attr('src','<?php echo $diretorio ?>ui/img/loading.gif');
                $('#logotipo1').attr('src','<?php echo $diretorio ?>ui/img/loading.gif');     
            },
            success: function(data) {
                $('#logotipo').attr('src',data);
                $('#logotipo1').attr('src',data);
            }
        });            
    }
    
    function loadFoto(foto) {
        var img = foto;
        $.ajax({
            url: 'controller/carregaLogo.php',
            type: 'POST',
            data: {
                img: img
            },
            beforeSend: function() {
                $('#fotoPerfil').attr('src','<?php echo $diretorio ?>ui/img/loading.gif');
                $('#fotoperfil').attr('src','<?php echo $diretorio ?>ui/img/loading.gif');     
                $('#foto_perfil').attr('src','<?php echo $diretorio ?>ui/img/loading.gif');     
            },
            success: function(data) {
                $('#fotoPerfil').attr('src',data);
                $('#fotoperfil').attr('src',data);                
                $('#foto_perfil').attr('src',data);                
            }
        });            
    }
    
    function salvarDados(data = null) {

        data = $("#form").serialize();
        $('#usuario').modal('hide');
            $.ajax({
                type: "POST",
                url: '<?php echo self::pag_Usuarios() ?>/add',
                data: data,
                success: function (response) {
                    $("#listar_dados").html(response);
                    /*swal({
                        position: 'center',
                        type: 'success',
                        title: 'Salvo com sucesso!',
                        showConfirmButton: false,
                        timer: 1700
                    });
                    $("#listar_dados").html(response);
                    if (document.getElementById('senha')) {
                        visulizarSenha();
                    }
                    $('#modal_dinamico').modal('hide');*/
                    //UIkit.notify("Operação realizada com sucesso", {pos: 'top-right', timeout: 2000, status: 'success'});
                }//,
                /*error: function (request, status, error) {
                    var texto = "";
                    if ($('.mensagem_erro').length) {
                        document.getElementById("div_mensagem_erro").style.display = 'block';
                        $('.mensagem_erro').html("Problema no cadastro:" + error);
                        setTimeout(function () {
                            document.getElementById("div_mensagem_erro").style.display = 'none';
                        }, 3000);
                    } else {
                        //UIkit.notify("Problema no cadastro:" + error, {pos: 'top-right', timeout: 2000, status: 'danger'});
                    }

                }*/
            });
    }
    
    $( document ).ready(function() {
        <?php
            $caminhoFoto = (isset($_SESSION['usuario_foto'])) ? $_SESSION['usuario_foto'] : '';
        ?>
        loadFoto("<?php echo $caminhoFoto ?>");
    });
    
    /*function editarPaciente(url){
        //alert(url);
        $.ajax({
           type: 'POST',
           url: url,
           data: {
               id: true
           },
           success: function(result){
               //$('#editar_paciente').modal('show');
               alert(result);
           }
        });
    }*/
    
    /*function salvarDados() {
        var data = $("#form").serialize();
        
        $.ajax({
            type: 'POST',
            url: "controler/paciente.php",
            data: data,
            success: function (result) {
                alert('Paciente salvo com sucesso!')
                $('#paciente').modal('hide');
            }
        });
    }*/
</script>

