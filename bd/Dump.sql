CREATE DATABASE  IF NOT EXISTS `av3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `av3`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: av3 
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dados_tabelas`
--

DROP TABLE IF EXISTS `dados_tabelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dados_tabelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabela_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dados_tabelas_tabela1_idx` (`tabela_id`),
  CONSTRAINT `fk_dados_tabelas_tabela1` FOREIGN KEY (`tabela_id`) REFERENCES `tabelas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dados_tabelas`
--

LOCK TABLES `dados_tabelas` WRITE;
/*!40000 ALTER TABLE `dados_tabelas` DISABLE KEYS */;
/*!40000 ALTER TABLE `dados_tabelas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `logo` varchar(100) DEFAULT 'ui/img/sem_imagem.jpg',
  `tipo` enum('Empresa','Autonomo') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
INSERT INTO `empresas` VALUES (1,'PSINWEB','','ui/logotipos/1_logo.png','Empresa');
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_usuario`
--

DROP TABLE IF EXISTS `log_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_usuario_usuarios1_idx` (`usuarios_id`),
  CONSTRAINT `fk_log_usuario_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_usuario`
--

LOCK TABLES `log_usuario` WRITE;
/*!40000 ALTER TABLE `log_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logotipo`
--

DROP TABLE IF EXISTS `logotipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logotipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_caminho` varchar(100) NOT NULL,
  `empresas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_logomarca_empresas1_idx` (`empresas_id`),
  CONSTRAINT `fk_logomarca_empresas1` FOREIGN KEY (`empresas_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logotipo`
--

LOCK TABLES `logotipo` WRITE;
/*!40000 ALTER TABLE `logotipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `logotipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo_dados_tabela`
--

DROP TABLE IF EXISTS `modelo_dados_tabela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo_dados_tabela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tabelas_modelo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dados_tabela_modelo_tabelas_modelo1_idx` (`tabelas_modelo_id`),
  CONSTRAINT `fk_dados_tabela_modelo_tabelas_modelo1` FOREIGN KEY (`tabelas_modelo_id`) REFERENCES `modelo_tabelas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo_dados_tabela`
--

LOCK TABLES `modelo_dados_tabela` WRITE;
/*!40000 ALTER TABLE `modelo_dados_tabela` DISABLE KEYS */;
/*!40000 ALTER TABLE `modelo_dados_tabela` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo_sessoes`
--

DROP TABLE IF EXISTS `modelo_sessoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo_sessoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `descricao` varchar(10000) NOT NULL,
  `modelos_relatorios_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sessoes_modelo_modelos_relatorios1_idx` (`modelos_relatorios_id`),
  CONSTRAINT `fk_sessoes_modelo_modelos_relatorios1` FOREIGN KEY (`modelos_relatorios_id`) REFERENCES `modelos_relatorios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo_sessoes`
--

LOCK TABLES `modelo_sessoes` WRITE;
/*!40000 ALTER TABLE `modelo_sessoes` DISABLE KEYS */;
INSERT INTO `modelo_sessoes` VALUES (1,'QUEIXA PRINCIPAL E MOTIVO DO ENCAMINHAMENTO','veio encaminhada para esse Serviço por neurologista a fim de investigar possível declínio cognitivo, lentidão das atividades mentais e esquecimentos. A paciente refere queixa de memória, dificuldades na escrita, sono irregular e perda de motivação para a realização de suas atividades diárias. Colateral pontua que a paciente tem dificuldade para se organizar e raras vezes apresenta dificuldade na linguagem.',1),(2,'DADOS RELEVANTES DA HISTÓRIA DE VIDA E CONDIÇÃO ATUAL','refere ter concluído o ensino médio aos 18 anos e relata não lembrar se apresentou dificuldades na aprendizagem.  Sua ocupação sempre fora dona de casa, cuidar dos filhos e dar aulas de crochê em igrejas e domicílios. Segundo relato, deixou de dar aulas há aproximadamente 30 anos, quando mudou seu endereço. Refere que há cerca de seis meses sua rotina mudou: deixou de fazer crochê e algumas atividades diárias. Relata sentir sono grande parte do dia e falta de motivação para iniciar atividades do dia-a-dia. Atualmente seu sono é irregular, pois acorda com frequência para urinar, às 3 horas, e não consegue voltar a dormir. XXXX paciente refere cair com grande frequência desde maio deste ano. Refere ainda que sente vertigem frequentemente. Colateral relata que XXX tem osteoporose, colesterol alto, alteração na tireoide, oscilação da pressão arterial, labirintite e catarata (em estágio inicial). Refere fazer uso das seguintes medicações: Puran (75mg); Alendronato de sódio (40mg); Osteofix; Sinvastatina (40mg); AS; Enalapril e Labirin. A paciente apresentou relatório de ressonância magnética de crânio realizada em 2016, apontando sinais de discreta redução volumétrica encefálica difusa, habitual para a faixa etária.',1),(3,'PROCEDIMENTO E PERÍODO DE AVALIAÇÃO','A avaliação neuropsicológica ocorreu nos dias XXXXXXXXX, em sessões de 50 minutos. A avaliação foi composta por entrevistas clínicas, aplicação de escalas e testagem neuropsicológica por meio dos seguintes instrumentos:\n\nEscala de Inteligência Wechsler para Adultos - WAIS III (NASCIMENTO, 2004);\nEscala Wechsler Abreviada de Inteligência – WASI (TRENTINI et al., 2014);\nTeste de Aprendizagem Auditivo-Verbal de Rey – RAVLT (SALGADO et al., 2010);\nTeste de Cópia e Reprodução de Memória de Figuras Geométricas Complexas (REY; OLIVEIRA; RIGONI, 2010);\nTeste dos Cubos de Corsi (MONACO et al., 2012);\nFive Digit Test – FDT (SEDÓ et al., 2015);\nTestes de Fluência Verbal Fonêmica – FAS (OPASSO, BARRETO & MARTINS, 2016);\nTeste de Desempenho Contínuo – CPT3 (Conners, 2015);\ndentre outras escalas e tarefas ecológicas.',1);
/*!40000 ALTER TABLE `modelo_sessoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo_tabelas`
--

DROP TABLE IF EXISTS `modelo_tabelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo_tabelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `sessoes_modelo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tabelas_modelo_sessoes_modelo1_idx` (`sessoes_modelo_id`),
  CONSTRAINT `fk_tabelas_modelo_sessoes_modelo1` FOREIGN KEY (`sessoes_modelo_id`) REFERENCES `modelo_sessoes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo_tabelas`
--

LOCK TABLES `modelo_tabelas` WRITE;
/*!40000 ALTER TABLE `modelo_tabelas` DISABLE KEYS */;
/*!40000 ALTER TABLE `modelo_tabelas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelos_relatorios`
--

DROP TABLE IF EXISTS `modelos_relatorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelos_relatorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelos_relatorios`
--

LOCK TABLES `modelos_relatorios` WRITE;
/*!40000 ALTER TABLE `modelos_relatorios` DISABLE KEYS */;
INSERT INTO `modelos_relatorios` VALUES (1,'Padrão');
/*!40000 ALTER TABLE `modelos_relatorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacientes`
--

DROP TABLE IF EXISTS `pacientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `sexo` enum('M','F') DEFAULT NULL,
  `dt_nascimento` date NOT NULL,
  `idade` int(4) DEFAULT NULL,
  `rg` varchar(11) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `n_prontuario` varchar(45) DEFAULT NULL,
  `ocupacao` varchar(45) DEFAULT NULL,
  `solicitante` varchar(100) DEFAULT NULL,
  `inicio_avaliacao` date DEFAULT NULL,
  `fim_avaliacao` date DEFAULT NULL,
  `dt_cadastro` datetime DEFAULT NULL,
  `usuarios_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pacientes_usuarios1_idx` (`usuarios_id`),
  CONSTRAINT `fk_pacientes_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacientes`
--

LOCK TABLES `pacientes` WRITE;
/*!40000 ALTER TABLE `pacientes` DISABLE KEYS */;
INSERT INTO `pacientes` VALUES (1,'Teste','M','2000-12-13',18,'1111111111','111.111.111-11','123','Analista','Teste Teste','2018-12-13','2019-01-13','2018-12-07 23:15:58',1);
/*!40000 ALTER TABLE `pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessoes`
--

DROP TABLE IF EXISTS `sessoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(10000) NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sessoes_pacientes1_idx` (`pacientes_id`),
  CONSTRAINT `fk_sessoes_pacientes1` FOREIGN KEY (`pacientes_id`) REFERENCES `pacientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessoes`
--

LOCK TABLES `sessoes` WRITE;
/*!40000 ALTER TABLE `sessoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabelas`
--

DROP TABLE IF EXISTS `tabelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `sessoes_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tabelas_sessoes1_idx` (`sessoes_id`),
  CONSTRAINT `fk_tabelas_sessoes1` FOREIGN KEY (`sessoes_id`) REFERENCES `sessoes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabelas`
--

LOCK TABLES `tabelas` WRITE;
/*!40000 ALTER TABLE `tabelas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabelas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `sobrenome` varchar(65) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `crp` varchar(10) DEFAULT NULL,
  `cargo` varchar(65) NOT NULL,
  `perfil` enum('Administrador','Usuario') NOT NULL,
  `pos_graduacao` enum('S','N') DEFAULT NULL,
  `pos_1` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT 'ui/img/user.png',
  `empresas_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_empresas_idx` (`empresas_id`),
  CONSTRAINT `fk_usuarios_empresas` FOREIGN KEY (`empresas_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'SAEB','Secretaria da Administração','saeb@gmail.com','111.111.111-11','202cb962ac59075b964b07152d234b70','111111111','Diretor','Administrador','N','','ui/fotoPerfil/1_foto.jpeg',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-06 14:42:41
