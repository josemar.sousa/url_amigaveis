<?php include 'ui/layout/topo.php'; ?>
    <script>
        <?php if(isset($_SESSION['erro'])){ ?>
            setTimeout(function(){ 
                $('#erroLogar').hide();
            }, 3000);
        <?php } ?>
    </script>
    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="login">
                <img src="ui/assets/pages/img/logo-big.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" id="formLogin"  method="post">
                <h3 class="form-title font-green">Acesso</h3>
                
                <div class="alert alert-danger display-hide text-center">
                    <button class="close" data-close="alert"></button>
                    <!--<span> Entre com seu nome de usuário e senha. </span>-->
                    <span> Entre com seu nome de usuário e senha. </span>
                </div>
                
                <?php if(isset($_SESSION['erro'])){ ?>
                <div id="erroLogar" class="alert alert-danger text-center">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $_SESSION['erro'] ?> </span>
                </div>
                <?php unset($_SESSION['erro']);} ?>
                
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">CPF</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" data-mask="999.999.999-99" autocomplete="off" placeholder="CPF" name="cpf" autofocus /> 
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Senha" name="password" /> 
                </div>
                <input type="text" name="efetuarLogin" value="true" hidden>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Acessar</button>
                    <!--<label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" />Lembrar
                        <span></span>
                    </label>-->
                    <a href="javascript:;" id="forget-password" class="forget-password">Esqueceu a senha?</a>
                </div>
                <!--<div class="login-options">
                    <h4>Ou faça o login com</h4>
                    <ul class="social-icons">
                        <li>
                            <a class="social-icon-color facebook" data-original-title="facebook" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color twitter" data-original-title="Twitter" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color googleplus" data-original-title="Goole Plus" href="javascript:;"></a>
                        </li>
                        <li>
                            <a class="social-icon-color linkedin" data-original-title="Linkedin" href="javascript:;"></a>
                        </li>
                    </ul>
                </div>-->
                <div class="create-account">
                    <p>
                        <a href="javascript:;" id="register-btn" class="uppercase">Criar Conta</a>
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="index.php" method="post">
                <h3 class="font-green">Esqueceu a senha ?</h3>
                <p> Digite seu email abaixo para redefinir sua senha. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Voltar</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Enviar</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
            <!-- BEGIN REGISTRATION FORM -->
            <form id="formCadastro" class="register-form" action="index" method="post">
                <h3 class="font-green">Criar sua Conta</h3><br>
                
                <div id="tipo_conta">
                    <p class="hint">Escolha um tipo de conta</p>
                    <div class="form-group form-md-radios">
                        <div class="md-radio-list">
                            <div class="md-radio">
                                <input type="radio" id="empresa" name="tipo_conta" value="1" class="md-radiobtn">
                                <label for="empresa">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Empresa </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="autonomo" name="tipo_conta" value="2" class="md-radiobtn">
                                <label for="autonomo">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Profissional Autônomo </label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="form_dados" style="display: none;">
                    <p class="hint"> Digite seus dados pessoais abaixo: </p>
                    
                    <div id="empresa_form" style="display: none">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Nome da Empresa</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="Nome da Empresa" name="nome" required="true"/> </div>
                        <div class="form-group">
                            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                            <label class="control-label visible-ie8 visible-ie9">Email</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" required="true" email="true" /> </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">CNPJ</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="CNPJ" name="cnpj" required="true" /> </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Direitor</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="Diretor" name="diretor" required="true" /> </div>
                    </div>
                    
                    <div id="autonomo_form" style="display: none">
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Nome Completo</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="Nome Completo" name="nome" required="true"/> </div>
                        <div class="form-group">
                            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                            <label class="control-label visible-ie8 visible-ie9">Email</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" required="true" email="true" /> </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">CPF</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="CPF" name="cpf" required="true" /> </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">CRP</label>
                            <input class="form-control placeholder-no-fix" type="text" placeholder="CRP" name="crp" required="true" /> </div>
                    </div>
                    
                    
                    <!--<div class="form-group mt-repeater">
                        <div data-repeater-list="group-c">
                            <div data-repeater-item class="mt-repeater-item">
                                <div class="row mt-repeater-row">
                                    <div class="col-md-8">
                                        <label class="control-label">Product Variation</label>
                                        <input type="text" placeholder="Salted Tuna" class="form-control" /> </div>
                                    <div class="col-md-1">
                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                            <i class="fa fa-plus"></i> Add Product Variation</a>
                    </div>-->

                    <p class="hint"> Insira os detalhes da sua conta abaixo: </p>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Usuário</label>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Usuário" name="login" required="true" /> </div>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Senha</label>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Senha" name="password" required="true" /> </div>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Digite novamente sua senha</label>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Digite novamente sua senha" name="rpassword" required="true" /> </div>
                    <div class="form-group margin-top-20 margin-bottom-20">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox" name="tnc"/> Eu concordo com os
                            <a href="javascript:;">Termos de serviço </a> &
                            <a href="javascript:;">Política de Privacidade </a>
                            <span></span>
                        </label>
                        <div id="register_tnc_error"> </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="register-back-btn" class="btn green btn-outline">Voltar</button>
                    <button type="submit" id="cadastrar_user" class="btn btn-success uppercase pull-right" disabled="">Cadastrar</button>
                </div>
            </form>
            <!-- END REGISTRATION FORM -->
        </div>
        <!--<div class="copyright"> 2018 © Av3. PSINWEB. </div>-->
    </body>
    <?php include 'ui/layout/footer.php'; ?>    
</html>