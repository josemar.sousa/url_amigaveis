<!-- Modal 1 Cadastrar Novo Usuário -->
<div id="usuario" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Novo Usuário</h4>
            </div>
            <div class="modal-body">
                <!--<div class="scroller" style="height:270px" data-always-visible="1" data-rail-visible1="1">-->
                <form id="form" method="post">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="nome" id="nome" required="true">
                                    <label for="nome">Nome</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="sobrenome" id="sobrenome" required="true">
                                    <label for="nome">Sobrenome</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="email" class="form-control" name="email" id="email" required="true">
                                    <label for="email">E-mail</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="cpf" id="cpf" data-mask="999.999.999-99" required="true">
                                    <label for="cpf">CPF</label>
                                </div>
                            </div>   
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="crp" id="crp"  required="true">
                                    <label for="rg">CRP</label>
                                </div>
                            </div>                                                 
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" name="perfil" id="perfil" required="true">
                                        <option value=""></option>
                                        <option value="Administrador">Administrador</option>
                                        <option value="Usuario">Usuário</option>
                                    </select>
                                    <label for="sexo">Perfil</label>
                                    <!--<span class="help-block">Some help goes here...</span>-->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="cargo" id="cargo" required="true">
                                    <label for="cargo">Cargo</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" name="pos_graduacao" id="pos_graduacao" required="true">
                                        <option value=""></option>
                                        <option value="S">Sim</option>
                                        <option value="N">Não</option>
                                    </select>
                                    <label for="sexo">Pós-Graduação</label>
                                    <!--<span class="help-block">Some help goes here...</span>-->
                                </div>
                            </div>
                            <div id="nomePos" style="display: none">
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="nome_pos" id="nome_pos">
                                        <label for="solicitante">Nome da Pós-Graduação</label>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name="empresas_id" hidden value="1">
                            <input type="text" name="cadastrar" hidden value="true">
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn dark btn-outline"><i class="fa fa-rotate-left"></i> Voltar</button>
                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </form>
                <!--</div>-->
            </div>
        </div>
    </div>
</div>


<!-- Modal 2 Editar Usuário -->
<div id="editar_usuario" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Novo Usuário</h4>
            </div>
            <div class="modal-body">
                <!--<div class="scroller" style="height:270px" data-always-visible="1" data-rail-visible1="1">-->
                <form id="form" method="post">
                        <div class="row">
                            <input type="text" id="id" name="id" hidden>
                            <div class="col-md-5">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="nome" id="nome" required="true" value="1">
                                    <label for="nome">Nome</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="sobrenome" id="sobrenome" required="true" value="1">
                                    <label for="sobrenome">Sobrenome</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="email" class="form-control" name="email" id="email" required="true" value="1">
                                    <label for="email">E-mail</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="cpf" id="cpf" data-mask="999.999.999-99" required="true" value="1">
                                    <label for="cpf">CPF</label>
                                </div>
                            </div>   
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="crp" id="crp"  required="true" value="1">
                                    <label for="rg">CRP</label>
                                </div>
                            </div>                                                 
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" name="perfil" id="perfil" required="true" value="1">
                                        <option value=""></option>
                                        <option value="Administrador" selected>Administrador</option>
                                        <option value="Usuario">Usuário</option>
                                    </select>
                                    <label for="sexo">Perfil</label>
                                    <!--<span class="help-block">Some help goes here...</span>-->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <input type="text" class="form-control" name="cargo" id="cargo" required="true" value="1">
                                    <label for="cargo">Cargo</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <select class="form-control" name="pos_graduacao" id="pos_graduacao" required="true">
                                        <option value=""></option>
                                        <option value="S" selected>Sim</option>
                                        <option value="N">Não</option>
                                    </select>
                                    <label for="sexo">Pós-Graduação</label>
                                    <!--<span class="help-block">Some help goes here...</span>-->
                                </div>
                            </div>
                            <div id="nomePos" style="display: none">
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="nome_pos" id="nome_pos" value="1">
                                        <label for="solicitante">Nome da Pós-Graduação</label>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name="empresas_id" hidden value="1">
                            <input type="text" name="editar" hidden value="true">
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn dark btn-outline"><i class="fa fa-rotate-left"></i> Voltar</button>
                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Editar</button>
                        </div>
                    </form>
                <!--</div>-->
            </div>
        </div>
    </div>
</div>
