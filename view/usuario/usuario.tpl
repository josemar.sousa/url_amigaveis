<?php include 'ui/layout/topo.php'; ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        
        <!-- MENU HORIZONTAL -->        
        <?php include 'ui/layout/menu-horizontal.php';?>
        <!-- END MENU HORIZONTAL -->
        
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            
            <!-- MENU VERTICAL -->
            <?php include 'ui/layout/menu-vertical.php';?>
            <!-- END MENU VERTICAL -->
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Usuários
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    
                    <!-- FORM - EXCLUIR PACIENTE-->
                    <form id="formExcluir" method="post">
                        <input type="text" name="excluir" id="excluir" hidden value="true">
                        <input type="text" name="id" id="id" hidden>
                    </form> 
                    <!-- END FORM - EXCLUIR PACIENTE-->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!-- Begin: life time stats -->
                            <div class="text-align-reverse" data-toggle="buttons">
                                <button class="btn green-dark" data-toggle="modal" data-target="#usuario" style="margin: 5px"><i class="fa fa-plus-circle"></i> Novo Usuário</button>
                            </div>
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet " style="margin: 0px; padding: 0px; display: none" id="voltar">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <a class="btn danger" href=""><i class="fa fa-reply"> Voltar</i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-users font-green"></i>
                                        <span class="caption-subject font-green sbold uppercase">Lista de Usuários</span>
                                    </div>
                                    <div class="actions">                                        
                                        <div class="btn-group">
                                            <a class="btn blue btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" id="exportar">
                                                <i class="fa fa-share"></i>
                                                <span class="hidden-xs"> Exportar </span>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right" id="sample_3_tools">
                                                <li>
                                                    <a href="javascript:;" data-action="0" class="tool-action">
                                                        <i class="icon-printer"></i> Imprimir</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" data-action="1" class="tool-action">
                                                        <i class="icon-check"></i> Copiar</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" data-action="2" class="tool-action">
                                                        <i class="icon-doc"></i> PDF</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" data-action="3" class="tool-action">
                                                        <i class="icon-paper-clip"></i> Excel</a>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <!--Mensagem de Sucesso-->
                                    <?php if(isset($_SESSION['sucesso'])){ ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <h4 class="alert-heading text-center"><?php echo $_SESSION['sucesso'] ?></h4>
                                    </div>
                                    <?php unset($_SESSION['sucesso']); } ?>
                                    
                                    <!--Mensagem de Erro-->
                                    <?php if(isset($_SESSION['erro'])){ ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <h4 class="alert-heading text-center"><?php echo $_SESSION['erro'] ?></h4>
                                    </div>
                                    <?php unset($_SESSION['erro']); } ?>                                    
                                </div>
                                
                                <div class="portlet-body">
                                    <div class="table-container" id="listar_dados">
                                        <?php include 'listar_dados.tpl' ?>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
    </body>
    <?php 
        include 'modals.tpl';
        include 'ui/layout/footer.php'; 
    ?>
    
    <script type="text/javascript">
        /*Modal Editar Usuário*/
        $('#editar_usuario').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var nome = button.data('nome')
            var sobrenome = button.data('sobrenome')
            var email = button.data('email')
            var cpf = button.data('cpf')
            var crp = button.data('crp')
            var perfil = button.data('perfil')
            var cargo = button.data('cargo')
            var pos_graduacao = button.data('pos_graduacao')
            var nome_pos = button.data('nome_pos')
            var empresas_id = button.data('empresas_id')

            var modal = $(this)
            modal.find('.modal-title').text('Editar Usuário ' /*+ nome*/)
            modal.find('#id').val(id)
            modal.find('#nome').val(nome)
            modal.find('#sobrenome').val(sobrenome)
            modal.find('#email').val(email)
            modal.find('#cpf').val(cpf)
            modal.find('#crp').val(crp)
            modal.find('#perfil').val(perfil)
            modal.find('#cargo').val(cargo)
            modal.find('#pos_graduacao').val(pos_graduacao)
            modal.find('#nome_pos').val(nome_pos)
            modal.find('#empresas_id').val(empresas_id)
            
        });
        
        if(voltar == true){
            $('#voltar').css('display','inline');            
        }
    </script>

</html>