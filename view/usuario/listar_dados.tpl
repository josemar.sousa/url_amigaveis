<table class="table table-striped table-bordered table-hover" id="sample_3">
    <thead>
        <tr>
            <th class="all"> Nome </th>
            <th> Cargo </th>
            <th> Perfil </th>
            <th class="all" style="width: 100px" id="acoes"> Ações </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($usuarios as $u){ ?>
        <tr>
            <td> <?php echo $u->nome; ?> </td>
            <td> <?php echo $u->cargo; ?> </td>
            <td> <?php echo $u->perfil; ?> </td>                                                    
            <td class="text-center" id="acoes">
                <!--<a type="button" class="btn btn-sm blue btn-outline tooltips" data-original-title="Ficha do Paciente"><i class="fa fa-edit" ></i></a>-->
                <a type="button" class="btn btn-sm green btn-outline tooltips"  data-original-title="Editar Usuário" data-toggle="modal" data-target="#editar_usuario" 
                        data-id="<?php echo $u->id ?>"
                        data-nome="<?php echo $u->nome ?>"
                        data-sobrenome="<?php echo $u->sobrenome ?>"
                        data-email="<?php echo $u->email ?>"
                        data-cpf="<?php echo $u->cpf ?>"
                        data-crp="<?php echo $u->crp ?>"
                        data-perfil="<?php echo $u->perfil ?>"
                        data-cargo="<?php echo $u->cargo ?>"
                        data-pos_graduacao="<?php echo $u->pos_graduacao ?>"
                        data-nome_pos="<?php echo $u->pos_1 ?>"
                        ><i class="fa fa-pencil" ></i></a>
                <a type="button" class="btn btn-sm red btn-outline tooltips" data-toggle="confirmation" data-funcao="excluir" data-id="<?php echo $u->id ?>"  data-original-title="Deseja realmente excluir este usuário?"><i class="fa fa-trash"></i></a>

            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>