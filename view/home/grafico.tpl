<script type="text/javascript">

function showChartTooltip(x, y, xValue, yValue) {
    $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 40,
        left: x - 40,
        border: '0px solid #ccc',
        padding: '2px 6px',
        'background-color': '#fff'
    }).appendTo("body").fadeIn(200);
}

if ($('#site_activities').size() != 0) {
    //site activities
    var previousPoint2 = null;
    $('#site_activities_loading').hide();
    $('#site_activities_content').show();
    
    <?php
        $anoAtual = date('Y');
        if(isset(Rotas::$pag[1]) && is_numeric(Rotas::$pag[1])){            
            if($anoAtual == Rotas::$pag[1]){
                $mes = date("m"); 
                $ultino_ano = $Paciente->ultimoAnoCadastroPaciente();
                $ano = $ultino_ano->ano;
            }else{
                $ano = Rotas::$pag[1];
                $mes = '';
            }
        }else{
            $mes = date("m"); 
            $ultino_ano = $Paciente->ultimoAnoCadastroPaciente();
            $ano = $ultino_ano->ano;
        }
        
        $mesAtual = $Paciente->mesAnoCadastroPaciente($mes, $ano);
    ?>

    var data1 = [
        ['JAN', <?php if($mes == 1 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 1 && !empty($mes)){}else{ $mesJaneiro = $Paciente->mesAnoCadastroPaciente(1, $ano); echo $mesJaneiro->total_cadastro_mes_ano;}?>],
        ['FEV', <?php if($mes == 2 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 2 && !empty($mes)){}else{ $mesFevereiro = $Paciente->mesAnoCadastroPaciente(2, $ano); echo $mesFevereiro->total_cadastro_mes_ano;}?>],
        ['MAR', <?php if($mes == 3 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 3 && !empty($mes)){}else{ $mesMarco= $Paciente->mesAnoCadastroPaciente(3, $ano); echo $mesMarco->total_cadastro_mes_ano;}?>],
        ['ABR', <?php if($mes == 4 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 4 && !empty($mes)){}else{ $mesAbril= $Paciente->mesAnoCadastroPaciente(4, $ano); echo $mesAbril->total_cadastro_mes_ano;}?>],
        ['MAI', <?php if($mes == 5 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 5 && !empty($mes)){}else{ $mesMaio= $Paciente->mesAnoCadastroPaciente(5, $ano); echo $mesMaio->total_cadastro_mes_ano;}?>],
        ['JUN', <?php if($mes == 6 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 6 && !empty($mes)){}else{ $mesJunho= $Paciente->mesAnoCadastroPaciente(6, $ano); echo $mesJunho->total_cadastro_mes_ano;}?>],
        ['JUL', <?php if($mes == 7 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 7 && !empty($mes)){}else{ $mesJulho= $Paciente->mesAnoCadastroPaciente(7, $ano); echo $mesJulho->total_cadastro_mes_ano;}?>],
        ['AGO', <?php if($mes == 8 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 8 && !empty($mes)){}else{ $mesAgosto= $Paciente->mesAnoCadastroPaciente(8, $ano); echo $mesAgosto->total_cadastro_mes_ano;}?>],
        ['SET', <?php if($mes == 9 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 9 && !empty($mes)){}else{ $mesSetembro= $Paciente->mesAnoCadastroPaciente(9, $ano); echo $mesSetembro->total_cadastro_mes_ano;}?>],
        ['OUT', <?php if($mes == 10 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 10 && !empty($mes)){}else{ $mesOutubro= $Paciente->mesAnoCadastroPaciente(10, $ano); echo $mesOutubro->total_cadastro_mes_ano;}?>],
        ['NOV', <?php if($mes == 11 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 11 && !empty($mes)){}else{ $mesNovembro= $Paciente->mesAnoCadastroPaciente(11, $ano); echo $mesNovembro->total_cadastro_mes_ano;}?>],
        ['DEZ', <?php if($mes == 12 && !empty($mes)){ echo $mesAtual->total_cadastro_mes_ano;}elseif($mes < 12 && !empty($mes)){}else{ $mesDezembro= $Paciente->mesAnoCadastroPaciente(12, $ano); echo $mesDezembro->total_cadastro_mes_ano;}?>]
    ];


    var plot_statistics = $.plot($("#site_activities"),

        [{
            data: data1,
            lines: {
                fill: 0.2,
                lineWidth: 0,
            },
            color: ['#BAD9F5']
        }, {
            data: data1,
            points: {
                show: true,
                fill: true,
                radius: 4,
                fillColor: "#9ACAE6",
                lineWidth: 2
            },
            color: '#9ACAE6',
            shadowSize: 1
        }, {
            data: data1,
            lines: {
                show: true,
                fill: false,
                lineWidth: 3
            },
            color: '#9ACAE6',
            shadowSize: 0
        }],

        {

            xaxis: {
                tickLength: 0,
                tickDecimals: 0,
                mode: "categories",
                min: 0,
                font: {
                    lineHeight: 18,
                    style: "normal",
                    variant: "small-caps",
                    color: "#6F7B8A"
                }
            },
            yaxis: {
                ticks: 5,
                tickDecimals: 0,
                tickColor: "#eee",
                font: {
                    lineHeight: 14,
                    style: "normal",
                    variant: "small-caps",
                    color: "#6F7B8A"
                }
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#eee",
                borderColor: "#eee",
                borderWidth: 1
            }
        });

    $("#site_activities").bind("plothover", function(event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));
        if (item) {
            if (previousPoint2 != item.dataIndex) {
                previousPoint2 = item.dataIndex;
                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1]);
            }
        }
    });

    $('#site_activities').bind("mouseleave", function() {
        $("#tooltip").remove();
    });
}

</script>