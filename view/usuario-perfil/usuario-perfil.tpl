<?php include 'ui/layout/topo.php'; ?>
<link href="ui/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        
        <!-- MENU HORIZONTAL -->        
        <?php include 'ui/layout/menu-horizontal.php';?>
        <!-- END MENU HORIZONTAL -->
        
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            
            <!-- MENU VERTICAL -->
            <?php include 'ui/layout/menu-vertical.php';?>
            <!-- END MENU VERTICAL -->
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Meu Perfil
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="tab-pane active">
                        <div class="row">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="tab-content">
                                <!-- 1 -->
                                <div class="col-md-4">
                                    <div class="portlet light profile-sidebar-portlet bordered">
                                        <!-- SIDEBAR USERPIC -->
                                        <div class="profile-userpic">
                                            <img src="<?php echo $usuarios->foto ?>" id="fotoperfil" class="img-responsive" alt="Foto de Perfil"> </div>
                                        <!-- END SIDEBAR USERPIC -->
                                        <!-- SIDEBAR USER TITLE -->
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name"> <?php echo $usuarios->nome ?> </div>
                                            <div class="profile-usertitle-job"> <?php echo $usuarios->cargo ?> </div>
                                        </div>
                                        <!-- END SIDEBAR USER TITLE -->
                                        <!-- SIDEBAR BUTTONS -->
                                        <!--
                                        <div class="profile-userbuttons">
                                            <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                            <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                        </div>
                                        -->
                                        <!-- END SIDEBAR BUTTONS -->
                                        <!-- SIDEBAR MENU -->
                                        <div class="tab-pane active profile-usermenu">
                                            <ul class="nav">
                                                <li class="<?php echo (!isset($activeFoto) && !isset($activeSenha)) ? 'active' : ''; ?>">
                                                    <a data-toggle="tab" href="#tab_1">
                                                        <i class="fa fa-edit"></i> Editar Perfil 
                                                    </a>
                                                </li>
                                                <li class="<?php echo (isset($activeFoto)) ? $activeFoto : ''; ?>">
                                                    <a data-toggle="tab" href="#tab_2">
                                                        <i class="icon-picture"></i> Alterar Imagem 
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tab" href="#tab_3">
                                                        <i class="icon-lock"></i> Alterar Senha 
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- END MENU -->
                                    </div>
                                </div>
                                <!-- END 1 -->
                                <div class="col-md-8">
                                    <!--Mensagem de Sucesso-->
                                    <?php if(isset($_SESSION['sucesso'])){ ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <h4 class="alert-heading text-center"><?php echo $_SESSION['sucesso'] ?></h4>
                                    </div>
                                    <?php unset($_SESSION['sucesso']); } ?>
                                    
                                    <!--Mensagem de Erro-->
                                    <?php if(isset($_SESSION['erro'])){ ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                        <h4 class="alert-heading text-center"><?php echo $_SESSION['erro'] ?></h4>
                                    </div>
                                    <?php unset($_SESSION['erro']); } ?>
                                    
                                    <div class="portlet light bordered">
                                        <div class="tab-content">
                                            <div id="tab_1" class="tab-pane <?php echo (!isset($activeFoto) && !isset($activeSenha)) ? 'active' : ''; ?>">
                                                <!--Tab 1 Editar Perfil-->
                                                <form method="post">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Nome</label>
                                                            <input type="nome" name="nome" value="<?php echo $usuarios->nome ?>" required="true" class="form-control" /> 
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Sobrenome</label>
                                                            <input type="sobrenome" name="sobrenome" value="<?php echo $usuarios->sobrenome ?>" required="true" class="form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">E-mail</label>
                                                            <input type="email" name="email" value="<?php echo $usuarios->email ?>" required="true" class="form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Matrícula</label>
                                                            <input type="text" name="crp" value="<?php echo $usuarios->crp ?>" required="true" class="form-control" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-5">
                                                            <label class="control-label">CPF</label>
                                                            <input type="text" name="cpf" value="<?php echo $usuarios->cpf ?>" data-mask="999.999.999-99" required="true" readonly class="form-control" /> 
                                                        </div>
                                                        <div class="form- col-md-5">
                                                            <label class="control-label">Cargo</label>
                                                            <input type="text" name="cargo" value="<?php echo $usuarios->cargo ?>" required="true" readonly class="form-control" /> 
                                                        </div>
                                                    </div>
                                                    <input type="text" name="id" hidden value="<?php echo $usuarios->id ?>">
                                                    <input type="text" name="editar" hidden value="true">
                                                    <div class="margin-top-30">
                                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Editar </button>
                                                    </div>
                                                </form>
                                                <!--END Tab 1 Editar Perfil-->
                                            </div>
                                            
                                            <!--Tab 2 - Adcionar/Alterar imagem--> 
                                            <div id="tab_2" class="tab-pane <?php echo (isset($activeFoto)) ? $activeFoto : ''; ?>">
                                                <div class="portlet-title">
                                                    <div class="caption font sbold"> Foto de Perfil </div>                                                        
                                                    <form id="formFoto" method="post" enctype="multipart/form-data">
                                                        <div class="form-group">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 300px;">
                                                                    <img src="<?php echo $usuarios->foto ?>" id="foto_perfil" alt="Foto de Perfil" /> </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                <div>
                                                                    <input type="text" name="id" id="id" value="<?php echo $usuarios->id ?>" hidden>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Alterar Imagem </span>
                                                                        <span class="fileinput-exists"> Alterar Imagem </span>
                                                                        <input type="file" name="fotoPerfil" id="fotoPerfil"> </span>
                                                                        <!--<input type="hidden" name="MAX_FILE_SIZE" value="1024000" />--> 
                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="margin-top-30">
                                                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Salvar </button>
                                                            <!--<a href="javascript:;" class="btn default"> Cancel </a>-->
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!--END Tab 2 - Adcionar/Alterar imagem-->
                                            
                                            <div id="tab_3" class="tab-pane <?php echo (isset($activeSenha)) ? $activeSenha : ''; ?>">
                                                <!--Tab 3 Alterar senha-->
                                                <form id="alterarSenha" class="register-form" method="post">
                                                    <div class="row">
                                                        <div class="form-group col-md-5">
                                                            <label class="control-label">Senha Atual</label>
                                                            <input type="password" name="senhaAtual" id="senhaAtual" class="form-control" required="true" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-5">
                                                            <label class="control-label">Nova Senha</label>
                                                            <input type="password" name="novaSenha" id="register_password" class="form-control" required="true" /> 
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-5">
                                                            <label class="control-label">Confirmar nova senha</label>
                                                            <input type="password" name="rpassword" class="form-control" required="true" /> 
                                                        </div>
                                                    </div>
                                                    <input type="text" name="id" hidden value="<?php echo $usuarios->id ?>">
                                                    <input type="text" name="cpf" hidden value="<?php echo $usuarios->cpf ?>">
                                                    <input type="text" name="alterarSenha" hidden value="true">
                                                    <div class="margin-top-30">
                                                        <button type="submit" class="btn green"><i class="fa fa-save"></i> Salvar </button>
                                                        <!--<a href="javascript:;" class="btn default"> Cancel </a>-->
                                                    </div>
                                                </form>
                                                <!--END Tab 3 Alterar senha-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                        </div>
                        <!-- END PAGE BASE CONTENT -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
    </body>
    <?php 
        include 'ui/layout/footer.php'; 
    ?>
    
    <script type="text/javascript">
        /*function alteraSenha() {
            var data = $("#alterarSenha").serialize();
            $.ajax({
                type: "POST",
                url: 'controler/usuario-perfil/alterarSenha.php',
                data: data,
                success: function (m) {
                    alert(m);
                }
            });
        }*/
    </script>

</html>