<?php include 'ui/layout/topo.php'; ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        
        <!-- MENU HORIZONTAL -->        
        <?php include 'ui/layout/menu-horizontal.php';?>
        <!-- END MENU HORIZONTAL -->
        
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            
            <!-- MENU VERTICAL -->
            <?php include 'ui/layout/menu-vertical.php';?>
            <!-- END MENU VERTICAL -->
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Empresa</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab" style="font-size: 25px"> Perfil </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!--Mensagem de Sucesso-->
                                <?php if(isset($_SESSION['sucesso'])){ ?>
                                <div class="alert alert-block alert-success fade in">
                                    <button type="button" class="close" data-dismiss="alert"></button>
                                    <h4 class="alert-heading text-center"><?php echo $_SESSION['sucesso'] ?></h4>
                                </div>
                                <?php unset($_SESSION['sucesso']); } ?>
                                
                                <!--Mensagem de Erro-->
                                <?php if(isset($_SESSION['erro'])){ ?>
                                <div class="alert alert-block alert-danger fade in">
                                    <button type="button" class="close" data-dismiss="alert"></button>
                                    <h4 class="alert-heading text-center"><?php echo $_SESSION['erro'] ?></h4>
                                </div>
                                <?php unset($_SESSION['erro']); } ?>

                                <div class="tab-pane active" id="tab_1_1">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                                <li style="display:flex; justify-content: center; height: 100px">
                                                    <img src="<?php echo $empresa->logo ?>" class="img-responsive pic-bordered" id="logotipo" alt="Logotipo" />
                                                    <!--<a href="javascript:;" class="profile-edit"> edit </a>-->
                                                </li>
                                                <li class="<?php echo (!isset($activePerfil) && !isset($activeLogo)) ? 'active' : ''; ?>" style="margin-top: 20px">
                                                    <a data-toggle="tab" href="#tab_1"><i class="fa fa-tachometer"></i>  Estatísticas </a>
                                                </li>
                                                <li class="<?php echo (isset($activePerfil)) ? $activePerfil : ''; ?>">
                                                    <a data-toggle="tab" href="#tab_2"><i class="fa fa-edit"></i> Editar Perfil </a>
                                                </li>
                                                <li class="<?php echo (isset($activeLogo)) ? $activeLogo : ''; ?>">
                                                    <a data-toggle="tab" href="#tab_3"><i class="fa fa-picture-o"></i> Alterar Imagem</a>
                                                </li>
                                                <li class="<?php echo (isset($activeSenha)) ? $activeSenha : ''; ?>">
                                                    <a data-toggle="tab" href="#tab_4"><i class="fa fa-lock"></i> Alterar Senha</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="row">   
                                                <div class="tab-content">
                                                    
                                                    <!--Tab 1 - Estatística-->
                                                    <div id="tab_1" class="tab-pane <?php echo (!isset($activePerfil) && !isset($activeLogo)) ? 'active' : ''; ?>">
                                                        <div class="col-md-12">
                                                            <div class="portlet sale-summary">
                                                                <div class="portlet-title">
                                                                    <div class="caption font-red sbold"> Estatísticas </div>
                                                                    <div class="tools">
                                                                        <a class="reload" href="javascript:;"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <ul class="list-unstyled">
                                                                        <li>
                                                                            <span class="sale-info"> USUÁRIOS CADASTRADOS</span>
                                                                            <span class="sale-num"> <?php echo $usuario->total ?> </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="sale-info"> PACIENTES EM AVALIAÇÃO</span>
                                                                            <span class="sale-num"> <?php echo $pacientesEmAvaliacao->em_avaliacao;?> </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="sale-info"> PACIENTES FINALIZADOS </span>
                                                                            <span class="sale-num"> <?php echo $pacientesFimAvaliacao->fim_avaliacao;?> </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="sale-info"> TOTAL DE PACIENTES </span>
                                                                            <span class="sale-num"> <?php echo $pacientesTotal->total;?> </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--END Tab 1 - Estatística-->
                                                    
                                                    <!--Tab 2 - Editar Pefil-->
                                                    <div id="tab_2" class="tab-pane col-md-8 <?php echo (isset($activePerfil)) ? $activePerfil : ''; ?>">
                                                        <form role="formEditaPerfil" method="post" action="empresa">
                                                            <div class="form-group">
                                                                <label class="control-label">Nome</label>
                                                                <input type="text" name="nome" value="<?php echo $empresa->nome ?>" required="true" class="form-control" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Descrição</label>
                                                                <textarea class="form-control" name="descricao" rows="8" maxlength="500" ><?php echo $empresa->descricao ?></textarea>
                                                            </div>
                                                            <input type="text" name="id" value="<?php echo $empresa->id ?>" hidden>
                                                            <input type="text" name="editarPerfil" value="true" hidden>
                                                            <div class="margin-top-30">
                                                                <button  type="submit" class="btn green"><i class="fa fa-save"></i> Salvar <i></i> </button>
                                                                <!--<a href="javascript:;" class="btn default"> Cancel </a>-->
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!--END Tab 2 - Editar Pefil-->
                                                    
                                                    <!--Tab 3 - Adcionar/Alterar imagem--> 
                                                    <div id="tab_3" class="tab-pane col-md-5 <?php echo (isset($activeLogo)) ? $activeLogo : ''; ?>">
                                                        <div class="portlet-title">
                                                            <div class="caption font sbold"> LOGOTIPO </div>                                                        
                                                            <form id="formLogo" method="post" enctype="multipart/form-data">
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 300px;">
                                                                            <img src="<?php echo $empresa->logo ?>" id="logotipo1" alt="LogoTipo" /> </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                        <div>
                                                                            <input type="text" name="id" id="id" value="<?php echo $empresa->id ?>" hidden>
                                                                            <span class="btn default btn-file">
                                                                                <span class="fileinput-new"> Alterar Imagem </span>
                                                                                <span class="fileinput-exists"> Alterar Imagem </span>
                                                                                <input type="file" name="logoTipo" id="logoTipo"> </span>
                                                                                <!--<input type="hidden" name="MAX_FILE_SIZE" value="1024000" />--> 
                                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="margin-top-30">
                                                                    <button type="submit" class="btn green"><i class="fa fa-save"></i> Salvar </button>
                                                                    <!--<a href="javascript:;" class="btn default"> Cancel </a>-->
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <?php
                                                            /*if(!empty($_FILES)){
                                                                $UploadLogo->width = 350;
                                                                $UploadLogo->height = 90;

                                                                echo $UploadLogo->salvar("ui/logotipos/", $_FILES['logoTipo'], $id_empresa);
                                                            }*/
                                                        ?>
                                                    </div>
                                                    <!--END Tab 3 - Adcionar/Alterar imagem-->
                                                    
                                                    <!--Tab 4 Alterar senha-->
                                                    <div id="tab_4" class="tab-pane col-md-8 <?php echo (isset($activeSenha)) ? $activeSenha : ''; ?>">
                                                        <form id="alterarSenha" class="register-form" method="post">
                                                            <div class="row">
                                                                <div class="form-group col-md-8">
                                                                    <label class="control-label">Senha Atual</label>
                                                                    <input type="password" name="senhaAtual" id="senhaAtual" class="form-control" required="true" /> 
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-8">
                                                                    <label class="control-label">Nova Senha</label>
                                                                    <input type="password" name="novaSenha" id="register_password" class="form-control" required="true" /> 
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-md-8">
                                                                    <label class="control-label">Confirmar nova senha</label>
                                                                    <input type="password" name="rpassword" class="form-control" required="true" /> 
                                                                </div>
                                                            </div>
                                                            <input type="text" name="id" hidden value="<?php echo $empresa->id ?>">
                                                            <input type="text" name="alterarSenha" hidden value="true">
                                                            <div class="margin-top-30">
                                                                <button type="submit" class="btn green"><i class="fa fa-save"></i> Salvar </button>
                                                                <!--<a href="javascript:;" class="btn default"> Cancel </a>-->
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!--END Tab 4 Alterar senha-->
                                                    
                                                </div>
                                            </div>
                                            <!--end row-->                                            
                                        </div>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
    </body>
    <?php 
        include 'ui/layout/footer.php'; 
    ?>
    
    <script type="text/javascript">
        $( document ).ready(function() {
            attImagem("<?php echo $empresa->logo ?>");
        });
    </script>

</html>